# Base image is node:10-slim
FROM node:10-slim

# Create and set the working directory to /inside-rx-website
WORKDIR /pdw

# Install dependencies
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y --no-install-recommends \
    redis-server \
    git \
    python3 \
    python3-pip \
    python3-setuptools \
  && pip3 install --upgrade pip \
  && apt-get clean \
  && apt-get install -y bzip2 \
  && apt-get install -y curl
# Install DOCKER
# Install docker cli
RUN curl -fL -o docker.tgz "https://download.docker.com/linux/static/stable/x86_64/docker-18.06.0-ce.tgz" \
 && tar --extract \
    --file docker.tgz \
    --strip-components 1 \
    --directory /usr/local/bin/ \
 && rm docker.tgz

# Install AWS CLI
RUN pip3 --no-cache-dir install --upgrade awscli

# Install ssh client
RUN apt-get install -y --no-install-recommends openssh-client

# Install NODE MODULES
RUN npm install

# Install ecs-deploy
RUN curl https://raw.githubusercontent.com/silinternational/ecs-deploy/master/ecs-deploy | tee -a /usr/local/bin/ecs-deploy && \
    chmod +x /usr/local/bin/ecs-deploy

# Add build/export script
# Base command
ENTRYPOINT [""]
