resource "aws_s3_bucket" "b" {
  bucket = "my-tf-test-bucket-pdw123"
  acl    = "private"

  tags = {
    Name        = "my-tf-test-bucket-pdw1234-20210621"
    Environment = "Dev"
  }
}
